import socket
from datetime import datetime

def prog(place, time):
    SERVER_IP = "34.218.16.79"
    SERVER_PORT = 77

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    server_address = (SERVER_IP, SERVER_PORT)
    sock.connect(server_address)
    #makeing the socket

    city = (place.split(","))[0] # the city alone

    checksum_after_point = 0 # the sum of all of the digits
    for i in time:
        if not (i == "/" or i == " "):
            checksum_after_point += int(i)

    checksum_before_point = 0 # the simple gematria of the city
    alphabet = {"a":1, "b":2, "c":3, "d":4, "e":5, "f":6, "g":7, "h":8, "i":9, "j":10, "k":11, "l":12, "m":13,
    "n":14, "o":15, "p":16, "q":17, "r":18, "s":19, "t":20, "u":21, "v":22, "w":23, "x":24, "y":25, "z":26, " ":0}
    for i in city.lower():
        if i in alphabet.keys():
            checksum_before_point += alphabet[i] # this wey we'll get the num equivalent of each letter

    # 100:REQUEST:city=Jerusalem&date=6/11/2021&checksum=104.13
    msg = "100:REQUEST:city=" + str(city) + "&date=" + str(time) + "&checksum=" + str(checksum_before_point) + "." + str(checksum_after_point)
    sock.sendall(msg.encode())

    server_msg = sock.recv(1024) # to get the msg back
    server_msg = sock.recv(1024) # to get the msg back

    server_msg = server_msg.decode()
    return server_msg

def print_answer(answer):
    if("200:ANSWER:" not in answer):
        print(999)
        print(answer)
    else:
        #27/04/2020, Temperature: 14.8, Rainy.
        parts = answer.split("=")
        full_date = (parts[1]).split("&")
        date = full_date[0]
        full_temp = (parts[3]).split("&")
        temp = full_temp[0]
        more = parts[4]
        msg = date + ", Temperature: " + temp + ", " + more
        print(msg)

def check_if_date_valid(time): # the func to check if the date is a real date (totaly not a code I took from my oter project (: )
    time_list = time.split("/")
    month = int(time_list[1])
    monthDays = 0
    if(month == 1 or month == 3 or month == 5 or month == 7 or month == 8 or month == 10 or month == 12):
        monthDays = 31
    elif(month == 2):
        monthDays = 28
    elif(month < 12):
        monthDays = 30
    else:
	    monthDays = -1
    #to find the the amount of days in the month (it wont work for leap year but I think it whould be just overkill at this point)
    # if it's still needed tell me and I can add it...
    if ((month > 12) or (month < 1) or (int(time_list[0]) > monthDays) or (int(time_list[0]) < 1)):
        return False # the date is invalid
    else:
        return True # the date is real

def menu(): # to get the menu
    print("1 - print the weather today")
    print("2 - print the weather today and the 3 next day")
    choice = input()
    while not (choice == "1" or choice == "2"):
        choice = input("wrong number, please enter again")
    return choice

def find_date_today(): # to get the date today
    today = str(datetime.now()) #the func returns things about the date today
    time_list = today.split(" ")
    time_now = time_list[0]
    day = (time_now.split("-"))
    return "/".join([day[2], day[1], day[0]])

def main():
    today = find_date_today()
    print("hello welcome to weather client (:")
    city = input("where do you live?\n")
    print("what do you want to know?")
    choice = menu()
    print_answer(prog(city, today))
    if(choice == "2"): # if it's 2 we print the date for the next 3 days
        for i in range(2):
            time = today.split("/")
            day = int(time[0])
            day += 1
            today = "/".join([str(day), time[1], time[2]])
            if not (check_if_date_valid(today)):
                time = today.split("/")
                day = 0
                month = int(today[1]) + 1
                today = "/".join([str(day), str(month), time[2]])
                if not (check_if_date_valid(today)):
                    time = today.split("/")
                    month = 0
                    year = int(today[2]) + 1
                    today = "/".join([time[0], str(month), str(year)])
            print_answer(prog(city, (today)))

if __name__ == "__main__":
    main()
