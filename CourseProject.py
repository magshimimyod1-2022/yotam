import random

def print_menu():
    print("""  _    _                                         
    | |  | |                                        
    | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  
    |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ 
    | |  | | (_| | | | | (_| | | | | | | (_| | | | |
    |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                        __/ |                      
                        |___/""")

    print(random.randint(5, 10))
"""
the func prints the home screen and return the number of guesses
:return: the guesses, a random num between 5 to 10 (int)
"""

def print_hangman(num_of_tries):
    dict = {
    1 : "x-------x",
    2 : """x-------x
    |
    |
    |
    |
    |""",
    3 : """x-------x
    |       |
    |       0
    |
    |
    |""",
    4 : """x-------x
    |       |
    |       0
    |       |
    |
    |
    """,
    5 : """x-------x
    |       |
    |       0
    |      /|\\
    |
    |
    """,
    6 : """x-------x
    |       |
    |       0
    |      /|\\
    |      /
    |
    """,
    7 : """x-------x
    |       |
    |       0
    |      /|\\
    |      / \\
    |
    """}

    print(dict[num_of_tries])

def choose_word(file_path, index):
    file = open(file_path, "r")
    txt = file.read()
    my_list = txt.split(" ")

    length = len(my_list) - 1
    index -= 1
    while index > length:
        index -= length + 1
    word = my_list[index]

    new_list = []
    counter = 0
    for i in range(len(my_list)):
        if my_list[i - counter] in new_list:
            new_list.append(my_list[i - counter])
            del my_list[i - counter]
            counter += 1
        else:
            new_list.append(my_list[i - counter])


    return (len(my_list), word)
"""
the func returns the word we need to find
:parm num1: the file with all of the words (file path)
:parm num2: the index of the word we need to find (int)
:return: the word we need to guess and the total number of words (tuple)
"""


def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    if ((not (letter_guessed.upper() in old_letters_guessed) and not (letter_guessed.lower() in old_letters_guessed)) and (len(letter_guessed) == 1 and letter_guessed.isalpha())):
        old_letters_guessed.append(letter_guessed)
        return True
    else:
        new = sorted(old_letters_guessed)
        new = (" -> ".join(new))
        print("X")
        print(new)
        return False
"""
the func checks if the guess is valid, and is so, adding it to the other guesses
:parm letter_guessed: the guess
:type letter_guessed: ste
:parm old_letters_guessed: all of the ither guesses
:type old_letters_guessed: list
:return: if it was valid
:rtype: bool
"""

def is_valid_input(letter_guessed):
    ans = False
    if (len(letter_guessed) == 1 and letter_guessed.isalpha()):
        ans = True
    return ans
"""
this func checks if the input is valid for our game
:parm letter_guessed: the player guess
:type letter_guessed: str
:return: if the guess is valid
:rtype: bool
"""

def show_hidden_word(secret_word, old_letters_guessed):
    new_str = ""
    for i in range(len(secret_word)):
        ans = False
        for j in range(len(old_letters_guessed)):
            if(secret_word[i] == old_letters_guessed[j]):
                ans = True
        if (ans):
            new_str += secret_word[i]
        else:
            new_str += "_"
        if i + 1 < len(secret_word):
            new_str += " "
    return new_str

def check_win(secret_word, old_letters_guessed):
    new_str = ""
    for i in range(len(secret_word)):
        ans = False
        for j in range(len(old_letters_guessed)):
            if(secret_word[i] == old_letters_guessed[j]):
                ans = True
        if (ans):
            new_str += secret_word[i]
        else:
            new_str += "_"
        if i + 1 < len(secret_word):
            new_str += " "
    if('_' in new_str):
        return False
    else:
        return True

def main():
    guess = print_menu()
    place = str(input("Enter file path: "))
    index = int(input("Enter index: "))
    word_tuple = choose_word(place, index)
    secret_word = word_tuple[1]
    old_letters_guessed = []
    counter = 2

    print("Let's start!\n")
    print(print_hangman(1))
    print(show_hidden_word(secret_word, old_letters_guessed))
    while not (check_win(secret_word, old_letters_guessed)) and counter <= 7:
        guess = input("Guess a letter: ")
        if (is_valid_input(guess or guess in old_letters_guessed)):
            if(guess in secret_word):
                try_update_letter_guessed(guess, old_letters_guessed)

            else:
                print(":(")
                if counter <= 7:
                    print_hangman(counter)
                counter += 1
            print(show_hidden_word(secret_word, old_letters_guessed))
        else:
            print("X")

    if(check_win(secret_word, old_letters_guessed)):
        print("WIN")
    else:
        print("LOSE")

if __name__ == "__main__":
    main()
