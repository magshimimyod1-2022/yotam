import requests

def files_list_makeing(str):
    line_html = str.split("\n")

    for i in range(8):
        del line_html[0]
    del line_html[-1]
    del line_html[-1]
    del line_html[-1]
    return line_html

def get_ending(line):
    return line[52:58]

def find_most_common_words():
    URL = "http://webisfun.cyber.org.il/nahman/final_phase//words.txt"
    file = requests.get(URL)
    html = file.text
    password = ""
    word_list = html.split(" ")

    for i in range(6):
        most_common = max(set(word_list), key = word_list.count)
        if password != "":
            password += " " + most_common
        else:
            password += most_common

        counter = 0
        for j in range(len(word_list)):
            if word_list[j - counter] == most_common:
                del word_list[j - counter]
                counter += 1

    return password


def extract_password_from_site():
    URL = "http://webisfun.cyber.org.il/nahman//files/"
    file = requests.get(URL)
    html = file.text

    line_html = files_list_makeing(html)

    password = ""

    for i in line_html:
        ending = get_ending(i)
        new_RL = URL + ending + ".nfo"
        file = requests.get(new_RL)
        new_html = file.text
        password += new_html[99]
    return password

def main():
    choice = int(input("choose a step: "))
    if choice == 1:
        print(extract_password_from_site())
    if choice == 2:
        print(find_most_common_words())

if __name__ == "__main__":
    main()
